<?php 
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != ""):
	{
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
		$database = "database.php";
	}
	if (isset($_GET['fid']))
	{
		$fid = $_GET['fid'];
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>
		<?php
			if (isset($_GET['fid']))
			{
				$fid = $_GET['fid'];
				require_once($database);
				$con = connect_database();
				$res = mysqli_query($con, "SELECT * FROM files WHERE fid = '$fid'");
				$file = mysqli_fetch_assoc($res);
				echo $file['nama']." | ".$file['kategori'];
			}
			else
				echo "Karya";
		?> | Showcase Karya Mahasiswa FTI UKDW
	</title>
	<link rel="stylesheet" type="text/css" href="stylesheet.css">
	<link rel="icon" href="resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="javascript.js"></script>
</head>
<body>
	<div class="header">
		<ul id="navleft">
			<li class="nav"><a href="index.php">BERANDA</a></li>
			<li class="nav"><a href="galeri.html">GALERI</a></li>
			<li class="nav"><a href="musik.html">MUSIK</a></li>
			<li class="nav"><a href="literatur.html">ARTIKEL</a></li>
			<li class="nav"><a href="lain.html">LAINNYA</a></li>
		</ul>
		<ul id="navright">
			<input class="nav" type="text" name="search" placeholder="Masukkan kata kunci">
			<li class="nav">CARI</li>
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn nav"><?php require_once($database); echo getName($nim); ?></button>
				<div id="myDropdown" class="dropdown-content">
					<?php if ($nim == 'admin' && $pass == 'admin'): ?>
						<a href="profil/admin.php">Profil</a>
					<?php else: ?>
						<a href="profil/profil.php?cat=">Profil</a>
						<a href="profil/profil.php?cat=upload">Upload Karya</a>
					<?php endif ?>
						<a href="setting.php?edit=akun&suc=">Setting</a>
						<a href="logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="profil.php?cat=home"><?php require_once($database); echo getName($nim); ?></a></li>
		</ul>
	</div>
	<div class="profil">
		<?php 
			require_once("database.php");
			$con = connect_database();
			$res = mysqli_query($con, "SELECT * FROM files WHERE fid = '$fid' AND status = 'approved'");
			$data = mysqli_fetch_assoc($res);
		?>
		<?php if ($data['kategori'] == 'Gambar'): ?>
		<div class="foto_gambar">
			<a href="<?php echo $data['scandir'].$data['filename']; ?>" target="_blank"><img src="<?php echo $data['icon']; ?>"></a>
		<?php else: ?>
		<div class="foto">
			<a href="<?php echo $data['scandir'].$data['filename']; ?>" target="_blank"><img src="<?php echo $data['icon']; ?>"></a>
		<?php endif ?>
		</div>
		<div class="overhead">
			<?php if (isset($_GET['fid'])): ?>
				<h1><?php echo $data['nama'];?></h1>
				<h3>Oleh : <a href="profil/profil.php?id=<?php echo $data['nim']; ?>&cat=home" target="_blank"><?php require_once("database.php"); echo getName($data['nim']);?></a></h5>
			<?php else: ?>
				<h1><?php require_once($database); echo getName($nim);?></h1>
				<h3>NIM : <?php echo $nim; ?></h3>
				<h5><?php require_once($database); echo getQuotes($nim);?></h5><span class="ganti"><a href="../setting.php?edit=profil&suc=">EDIT QUOTE</a></span>
			<?php endif ?>
		</div>
		<div class="profil-karya">
			<div class="kategori1"><a href="profil.php?id=<?php echo $uid; ?>&cat=karya">DESKRIPSI</a></div>
		</div>
		<?php
			echo $data['desc'];
		?>
	</div>
    <footer>
		&copy;SakitPantat. <a href="tentang_kami.html">Tentang kami.</a> <a href="bantuan.html">Bantuan.</a>
	</footer>
</body>
</html>
<?php 
	else:
		header("Location: ../login.html");
	endif?>