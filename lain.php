<?php
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != "" && $_SESSION["LOGIN"] == "true")
	{
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
		$database = "database.php";
	}

	function getIcon($con, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE kategori = 'Lainnya' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		while ($data = mysqli_fetch_assoc($res))
		{
			$files = scandir($data['scandir']);
			foreach ($files as $key => $file):
				if($key == 0 || $key == 1) ;
				else if ($file == $data['filename'])
					{echo "<td><a href = 'karya.php?fid=".$data['fid']."' target = '_blank'><img src='".$data['icon']."'></a>";}
			endforeach;
		}
	}

	function getAuthors($con, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE kategori = 'Lainnya' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
			{
				echo "<td>Oleh : <a href='profil.php?id=".$data['nim']."&cat=home' style='font-weight: bold;'>";
				require_once("database.php");
				echo getName($data['nim'])."</a></td>";
			}
		}
	}

	function getTitle($con, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE kategori = 'Lainnya' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
				echo "<td><a href='karya.php?fid=".$data['fid']."' target='_blank'>".$data['nama']."</td>";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lainnya | Showcase Karya Mahasiswa FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
	<link rel="icon" href="resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="javascript.js"></script>
</head>
<body onload="logout(<?php if (isset($_GET['out'])) echo $_GET['out']; ?>)">
	<div class="header">
		<ul id="navleft">
			<li class="nav"><a href="index.php">BERANDA</a></li>
			<li class="nav"><a href="galeri.php">GALERI</a></li>
			<li class="nav"><a href="musik.php">MUSIK</a></li>
			<li class="nav"><a href="literatur.php">ARTIKEL</a></li>
			<li class="nav"><a href="lain.php">LAINNYA</a></li>
		</ul>
		<ul id="navright">
			<input class="nav" type="text" name="search" placeholder="Masukkan kata kunci">
			<li class="nav">CARI</li>
			<?php if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != ""):?>
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn nav"><?php require_once("database.php"); echo getName($nim); ?></button>
				<div id="myDropdown" class="dropdown-content">
					<?php if ($nim == 'admin'): ?>
						<a href="profil/admin.php">Profil</a>
					<?php else: ?>
						<a href="profil/profil.php?cat=home">Profil</a>
						<a href="profil/profil.php?cat=upload">Upload Karya</a>
					<?php endif ?>
					<a href="setting.php?edit=akun&suc=">Setting</a>
					<a href="logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="profil/profil.php?cat=home"><?php require_once("database.php"); echo getName($nim); ?></a></li>
			<?php else:?>
			<li class="nav"><a href="login.html">MASUK</a></li>
		<?php endif?>
		</ul>
	</div>
	<div id="judul-lainnya">
		SHOWCASE MAHASISWA<br>FAKULTAS TEKNOLOGI INFORMASI<div id="ukdw">UNIVERSITAS KRISTEN DUTA WACANA</div>
	</div>
	<div class="kategori"><a href="galeri.php">GALERI</a></div>
		<?php $con = connect_database(); ?>
		<table id="artikel-index"><tbody>
		<?php $res = mysqli_query($con, "SELECT COUNT(fid) FROM files WHERE category = 'Lainnya' AND status = 'approved'");
		require_once($database);
		$uploaded = countUploadedCat('Lainnya');
		if ($uploaded <= 0) : ?>
			<tr class='showcase-title'><td>Belum ada karya yang diupload</td></tr>
		<?php else :?>
			<?php while ($uploaded > 0) :
				require_once($database);
				$off = countUploadedCat('Lainnya') - $uploaded; ?>
				<tr class='galeri-image'><?php getIcon($con, $off); ?></tr>
				<tr class='showcase-title'><?php getTitle($con, $off); ?></tr>
				<tr class='showcase-author'><?php getAuthors($con, $off); ?></tr>
				<?php $uploaded-=5; ?>
			<?php endwhile ?>
		<?php endif ?>
		</tbody></table>
	<footer>
		&copy;SakitPantat. <a href="tentang_kami.html">Tentang kami.</a> <a href="bantuan.html">Bantuan.</a>
	</footer>
</body>
</html>