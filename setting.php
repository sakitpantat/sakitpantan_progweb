<?php
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != "" && $_SESSION["LOGIN"] == "true")
	{
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
		$database = "database.php";
	}
	else
		header('Location:login.html');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Settings</title>
	<link rel="stylesheet" type="text/css" href="stylesheet.css">
	<link rel="icon" href="resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="javascript.js"></script>
</head>
<body onload="return settingSuccess(<?php echo $_GET['suc']; ?>)">
	<div class="header">
		<ul id="navright">
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn"><?php require_once($database); echo getName($nim);?></button>
				<div id="myDropdown" class="dropdown-content">
					<?php if ($nim == 'admin'): ?>
						<a href="profil/admin.php">Profil</a>
					<?php else: ?>
						<a href="profil/profil.php?cat=home">Profil</a>
						<a href="profil/profil.php?cat=upload">Upload Karya</a>
					<?php endif ?>
					<a href="setting.php?edit=akun&suc=">Setting</a>
					<a href="logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="profil/profil.php?cat=home"><?php require_once("database.php"); echo getName($nim); ?></a></li>
		</ul>
	</div>
	<div class="setting-left">
		<table class="setting-cat">
			<tr><td><a href="setting.php?edit=akun&suc=">Akun - <?php require_once("database.php"); echo $nim; ?></a></td></tr>
			<?php if ($nim != 'admin'): ?>
				<tr><td><a href="setting.php?edit=profil&suc=">Profil</a></td></tr>
			<?php endif ?>
		</table>
	</div>
	<div class="setting-right">
		<table class="formfill">
			<?php
				if ($_GET['edit'] == 'akun'){
					echo 
					'<form id="edit-akun" action="change.php?edit=akun" method="post" onsubmit="return repassCheck()" name="edit">
					
					<tr>
						<td class style="text-align:center;">Ubah Password</td>
					</tr>
					<tr>
						<td><label class="aturpassword">Password Lama</label></td>
						<td><input class = "isiform" type="password" name="pass-old" placeholder="Ketik Password Lama"></td>
					</tr>
						<td><label class="aturpassword">Password Baru</label></td>
						<td><input class = "isiform" type="password" name="pass-new" placeholder="Ketik Password Baru"></td>
					<tr>
						<td><label class="aturpassword">Ulang Password</label></td>
						<td><input class = "isiform" type="password" name="pass-re" placeholder="Ketik Ulang Password Baru"></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"></td>
						<td><input type="reset"></td>
					</tr></form>';
				}
				else if ($_GET['edit'] == 'profil')
				{
					require_once("database.php");
					$con = connect_database();
					$hasil = mysqli_query($con, "SELECT * FROM biodatas WHERE nim LIKE '$nim'");
					while ($data = mysqli_fetch_assoc($hasil))
					{
						echo
						'<form id="edit-profil" action="change.php?edit=profil" method="post" enctype="multipart/form-data">
						<tr>
						<td><label class="aturpassword">Quotes :</label></td>
						<td><input type="text" name="quote" placeholder="Quotes" value="'.$data['quotes'].'"></td>
						</tr>
						<tr>
						<td><label class="aturpassword">Alamat :</label></td>
						<td><input type="text" name="alamat" placeholder="Alamat" value="'.$data['alamat'].'"></td>
						</tr>
						<tr>
							<td><label class="aturpassword">Email Utama :</label></td>
							<td>'.$data['email'].'</td>
						</tr>
						<tr>
							<td><label class="aturpassword">Email Cadangan :</label></td>
							<td><input type="email" name="sub_email" placeholder="Email Cadangan" value="'.$data['sub_email'].'"></td>
						</tr>
						<tr><td>(Optional)</td></tr>
						<td><label class="aturpassword">Nomor HP :</label></td>
						<td><input type="text" name="no_hp" placeholder="Nomor HP" value="'.$data['no_hp'].'"></td>
						<tr colspan=2><td><label class="aturpassword">Ganti Foto Profil</label></td>
						<tr><td><label class="aturpassword">Pilih file :</label></td>
						<td><input type="file" name="up"></td></tr>
						<tr>
							<td><input type="submit"></a></td>
							<td><input type="reset"></td>
						</tr></form>';
					}
				}
			?>
		</table>
	</div>
	<br>
    <br>
</body>
</html>