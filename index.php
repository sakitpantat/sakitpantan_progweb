<?php
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != "" && $_SESSION["LOGIN"] == "true")
	{
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
	}

	function getIcon($con, $cat)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE kategori = '$cat' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5");
		while ($data = mysqli_fetch_assoc($res))
		{
			$files = scandir($data['scandir']);
			foreach ($files as $key => $file):
				if($key == 0 || $key == 1) ;
				else if ($file == $data['filename'])
					{echo "<td><a href = 'karya.php?fid=".$data['fid']."' target = '_blank'><img src='".$data['icon']."'></a>";}
			endforeach;
		}
	}

	function getAuthors($con, $cat)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE kategori = '$cat' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
			{
				echo "<td>Oleh : <a href='profil/profil.php?id=".$data['nim']."&cat=home' style='font-weight: bold;'>";
				require_once("database.php");
				echo getName($data['nim'])."</a></td>";
			}
		}
	}

	function getTitle($con, $cat)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE kategori = '$cat' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
				echo "<td><a href='karya.php?fid=".$data['fid']."' target='_blank'>".$data['nama']."</td>";
		}
		else
		{
			if ($cat == "Artikel")
				echo '<td>Belum ada karya dengan kategori ini yang diupload. Mulailah upload karya anda <a href="profil/profil.php?cat=upload&ref=artikel" style="text-decoration:underline;">disini</a></td>';
			else if ($cat == "Musik")
				echo '<td>Belum ada karya dengan kategori ini yang diupload. Mulailah upload karya anda <a href="profil/profil.php?cat=upload&ref=musik" style="text-decoration:underline;">disini</a></td>';
			else if ($cat == "Lainnya")
				echo '<td>Belum ada karya dengan kategori ini yang diupload. Mulailah upload karya anda <a href="profil/profil.php?cat=upload&ref=lainnya" style="text-decoration:underline;">disini</a></td>';
			else if ($cat == "Gambar")
				echo '<td>Belum ada karya dengan kategori ini yang diupload. Mulailah upload karya anda <a href="profil/profil.php?cat=upload&ref=gambar" style="text-decoration:underline;">disini</a></td>';
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Showcase Karya Mahasiswa FTI UKDW</title>
	<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
	<link rel="icon" href="resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="javascript.js"></script>
</head>
<body onload="logout(<?php if (isset($_GET['out'])) echo $_GET['out']; ?>)">
	<div class="header">
		<ul id="navleft">
			<li class="nav"><a href="index.php">BERANDA</a></li>
			<li class="nav"><a href="galeri.php">GALERI</a></li>
			<li class="nav"><a href="musik.html">MUSIK</a></li>
			<li class="nav"><a href="literatur.html">ARTIKEL</a></li>
			<li class="nav"><a href="lain.html">LAINNYA</a></li>
		</ul>
		<ul id="navright">
			<input class="nav" type="text" name="search" placeholder="Masukkan kata kunci">
			<li class="nav">CARI</li>
			<?php if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != ""):?>
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn nav"><?php require_once("database.php"); echo getName($nim); ?></button>
				<div id="myDropdown" class="dropdown-content">
					<?php if ($nim == 'admin'): ?>
						<a href="profil/admin.php">Profil</a>
					<?php else: ?>
						<a href="profil/profil.php?cat=home">Profil</a>
						<a href="profil/profil.php?cat=upload">Upload Karya</a>
					<?php endif ?>
					<a href="setting.php?edit=akun&suc=">Setting</a>
					<a href="logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="profil/profil.php?cat=home"><?php require_once("database.php"); echo getName($nim); ?></a></li>
			<?php else:?>
			<li class="nav"><a href="login.html">MASUK</a></li>
		<?php endif?>
		</ul>
	</div>
	<div id="judul">
		SHOWCASE MAHASISWA<br>FAKULTAS TEKNOLOGI INFORMASI<div id="ukdw">UNIVERSITAS KRISTEN DUTA WACANA</div>
	</div>
	<div class="kategori"><a href="galeri.html">GALERI</a></div>
	<?php
		require_once("database.php");
		$con = connect_database();
		$kategori = array("Gambar", "Musik", "Artikel", "Lainnya");
		$i = 0;
		$cat = $kategori[$i];
	?>
	<table id="galeri-index">
		<tr class="galeri-image">
			<?php getIcon($con, $cat); ?>
		</tr>
		<tr class="showcase-title">
			<?php getTitle($con, $cat); ?>
		</tr>
		<tr class="showcase-author">
			<?php getAuthors($con, $cat); $i++; $cat = $kategori[$i];?>
		</tr>
		<tr class="selengkapnya">
			<td colspan="5"><a href="galeri.html"><br>LIHAT SELENGKAPNYA ></a></td>
		</tr>
	</table>
	<div class="kategori"><a href="musik.html">MUSIK</a></div>
	<table id="musik-index">
		<tr class="musik-icon">
		<?php getIcon($con, $cat); ?>
		</tr>
		<tr class="showcase-title">
		<?php getTitle($con, $cat); ?>
		</tr>
		<tr class="showcase-author">
		<?php getAuthors($con, $cat); $i++; $cat = $kategori[$i];?>
		</tr>
		<tr class="selengkapnya">
			<td colspan="5"><a href="musik.html"><br>LIHAT SELENGKAPNYA ></a></td>
		</tr>
	</table>
	<div class="kategori"><a href="literatur.html">ARTIKEL</a></div>
	<table id="artikel-index">
		<tr class="artikel-icon">
			<?php getIcon($con, $cat); ?>
		</tr>
		<tr class="showcase-title">
			<?php getTitle($con, $cat); ?>
		</tr>
		<tr class="showcase-author">
			<?php getAuthors($con, $cat); $i++; $cat = $kategori[$i];?>
		</tr>
		<tr class="selengkapnya">
			<td colspan="5"><a href="literatur.html"><br>LIHAT SELENGKAPNYA ></a></td>
		</tr>
	</table>
	<div class="kategori"><a href="lain.html">LAINNYA</a></div>
	<table id="lain-index">
		<tr class="lain-icon">
			<?php getIcon($con, $cat); ?>
		</tr>
		<tr class="showcase-title">
			<?php getTitle($con, $cat); ?>
		</tr>
		<tr class="showcase-author">
			<?php getAuthors($con, $cat); $i++;?>
		</tr>
		<tr class="selengkapnya">
			<td colspan="5"><a href="lain.html"><br>LIHAT SELENGKAPNYA ></a></td>
		</tr>
	</table>
	<footer>
		&copy;SakitPantat. <a href="tentang_kami.html">Tentang kami.</a> <a href="bantuan.html">Bantuan.</a>
	</footer>
</body>
</html>