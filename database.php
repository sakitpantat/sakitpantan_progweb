<?php
function connect_database() {
	$db_user = "progweb";
	$db_password = "progweb";
	$db_name = "progweb";
	$db_host = "localhost";
	
	$con = mysqli_connect($db_host, $db_user, $db_password, $db_name) or die("Failed to connect to database");
	return $con;
}

function getName($nim)
{
	$con = connect_database();
	$query = "SELECT nama FROM users WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $nama);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	return $nama;
}

function getQuotes($nim)
{
	$con = connect_database();
	$query = "SELECT quotes FROM biodatas WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $quote);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	return $quote;
}

function getEmail($nim)
{
	$con = connect_database();
	$query = "SELECT email FROM biodatas WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $email);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	return $email;
}

function getEmailSub($nim)
{
	$con = connect_database();
	$query = "SELECT sub_email FROM biodatas WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $subemail);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	if ($subemail == "")
		return "-";
	else
		return $subemail;
}

function getAddress($nim)
{
	$con = connect_database();
	$query = "SELECT alamat FROM biodatas WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $address);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	if ($address == "")
		return "-";
	else
		return $address;
}

function getBirthday($nim)
{
	$con = connect_database();
	$query = "SELECT birthday FROM biodatas WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $birthday);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	return $birthday;
}

function countUploaded($nim)
{
	$con = connect_database();
	$query = "SELECT COUNT(filename) FROM files WHERE nim = '$nim' AND status = 'approved'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $count);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	return $count;
}

function countUploadedCat($cat)
{
	$con = connect_database();
	$query = "SELECT COUNT(fid) FROM files WHERE kategori = '$cat' AND status = 'approved'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $count);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	return $count;
}

function getNoHP($nim)
{
	$con = connect_database();
	$query = "SELECT no_hp FROM biodatas WHERE nim = '$nim'";
	if($stmt = mysqli_prepare($con, $query))
	{
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $no_hp);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
	}
	mysqli_close($con);
	if ($no_hp == "")
		return "-";
	else
		return $no_hp;
}