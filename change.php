<?php
	session_start();
	require("database.php");
	$con = connect_database();
	$nim = $_SESSION["NIM"];
	$pass = $_SESSION["PASS"];

	if ($_GET["edit"] == 'akun')
	{
		$pass_old = mysqli_real_escape_string($con, $_POST["pass-old"]);
		$pass_new = mysqli_real_escape_string($con, $_POST["pass-new"]);
		$pass_re = mysqli_real_escape_string($con, $_POST["pass-re"]);
		$sub_email = mysqli_real_escape_string($con, $_POST["sub_email"]);
		if ($pass_old == $pass)
		{
			if ($pass_new == $pass_re)
			{
				$update = mysqli_query($con, "UPDATE `users` SET `password` = AES_ENCRYPT('$pass_new','$nim') WHERE `users`.`nim` = '$nim'");
				$hasil = mysqli_query($con,"SELECT * FROM users WHERE nim='$nim' AND password=AES_ENCRYPT('$pass',nim)");
				$_SESSION["PASS"] = $pass_new;
				header("Location:setting.php?edit=akun&suc=1");
			}
			else
				echo "New != re";
			if (isset($sub_email))
			{
				$update = mysqli_query($con, "UPDATE `biodatas` SET `sub_email` = '$sub_email' WHERE `biodatas`.`nim` = '$nim'");
				header("Location:setting.php?edit=akun&suc=1");
			}
			else
				echo "email error";
		}
	}
	else if ($_GET["edit"] == 'profil')
	{
		$quotes = mysqli_real_escape_string($con, $_POST["quote"]);
		$alamat = mysqli_real_escape_string($con, $_POST["alamat"]);
		$no_hp = mysqli_real_escape_string($con, $_POST["no_hp"]);
		$sub_email = mysqli_real_escape_string($con, $_POST["sub_email"]);
		$query = "UPDATE `biodatas` SET `alamat` = '$alamat', `sub_email` = '$sub_email', `no_hp` = '$no_hp', `quotes` = '$quotes' WHERE `biodatas`.`nim` = '$nim'";
		if ($update = mysqli_query($con, $query))
		{
			if (is_uploaded_file($_FILES['up']['tmp_name']))
			{
				$nim = $_SESSION["NIM"];
				require_once("database.php");
				$con = connect_database();
				$file = $_FILES["up"];
				$ext = pathinfo($file["name"], PATHINFO_EXTENSION);
				$ext = strtolower($ext);
				$directory = "profil/foto/".$nim.".".$ext;
				$delete = mysqli_query($con, "SELECT * FROM users WHERE nim = '$nim'");
				$data = mysqli_fetch_assoc($delete);
				$nama = $data['pp'];
				if ($nama != "default.png")
				{
					$path = "profil/foto/".$nama;
					$file = glob($path);
					unlink($file[0]);
				}
				$update = mysqli_query($con, "UPDATE `users` SET `pp` = CONCAT('$nim', '.', '$ext') WHERE `users`.`nim` = '$nim'");
				if (move_uploaded_file($_FILES["up"]["tmp_name"], $directory))
					header("Location:setting.php?edit=profil&suc=2");
			}
			else
				header("Location:setting.php?edit=profil&suc=2");
		}
	}