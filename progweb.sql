-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2016 at 09:38 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `biodatas`
--

CREATE TABLE `biodatas` (
  `nim` varchar(16) NOT NULL,
  `nama` varchar(99) NOT NULL,
  `alamat` varchar(99) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(99) NOT NULL,
  `sub_email` varchar(99) DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  `quotes` varchar(99) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biodatas`
--

INSERT INTO `biodatas` (`nim`, `nama`, `alamat`, `birthday`, `email`, `sub_email`, `no_hp`, `quotes`) VALUES
('71140021', 'Satrio Adi Prakoso', 'Jalan jalan', '1996-05-02', 'satrio.ap@ti.ukdw.ac.id', NULL, NULL, 'Halo semua'),
('71140095', 'Wisnu Pramuditya', 'Jalan Magelang KM 4.5', '1997-03-06', 'wisnupramuditya@ti.ukdw.ac.id', 'evolution13th@gmail.com', '081227387556', 'It''s all relatives'),
('informatika', 'Informatika UKDW', 'Kampus UKDW', '1966-05-02', 'informatika@ti.ukdw.ac.id', '', '', 'TI AMPUH!! Aktif Mandiri Profesional Unggul Humanis');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `fid` int(1) NOT NULL,
  `nim` varchar(16) NOT NULL,
  `filename` varchar(99) NOT NULL,
  `scandir` varchar(99) NOT NULL,
  `nama` varchar(99) NOT NULL,
  `kategori` varchar(99) NOT NULL,
  `format` char(5) NOT NULL,
  `status` set('approved','pending') NOT NULL DEFAULT 'pending',
  `icon` varchar(99) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`fid`, `nim`, `filename`, `scandir`, `nama`, `kategori`, `format`, `status`, `icon`) VALUES
(32, '71140021', '13-05-2016_07-03-41_71140021.docx', 'artikel/', 'Goodbye Piracy', 'Artikel', 'docx', 'approved', 'resources/txt-icon.png'),
(38, 'informatika', '16-05-2016_09-19-42_informatika.png', 'galeri/design/', 'Natal', 'Gambar', 'png', 'approved', 'galeri/design/16-05-2016_09-19-42_informatika.png'),
(39, '71140095', '16-05-2016_09-21-32_71140095.png', 'galeri/design/', 'Gong Xi Fa Cai', 'Gambar', 'png', 'approved', 'galeri/design/16-05-2016_09-21-32_71140095.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `nim` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `nama` varchar(99) NOT NULL,
  `status` set('active','disabled') NOT NULL,
  `pp` varchar(99) NOT NULL DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`nim`, `password`, `nama`, `status`, `pp`) VALUES
('71140021', '?Z‰¸+É!Ò—#Ö?°Ii', 'Satrio Adi Prakoso', 'active', '71140021.png'),
('71140095', '·!œ÷€¡i×m<‘X³«Ý', 'Wisnu Pramuditya', 'active', '71140095.png'),
('admin', 'ÖCN»¦KIÈ@Îs?ç”', 'admin', 'active', 'default.png'),
('informatika', '&*Ç–Î„OG".üFØ', 'Informatika UKDW', 'active', 'informatika.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biodatas`
--
ALTER TABLE `biodatas`
  ADD PRIMARY KEY (`nim`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`fid`),
  ADD UNIQUE KEY `filename` (`filename`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`nim`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `fid` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
