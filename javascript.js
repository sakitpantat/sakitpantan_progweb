function repassCheck(){
	var passOld = document.forms["edit"]["pass-old"].value;
	var passNew = document.forms["edit"]["pass-new"].value;
	var passRe = document.forms["edit"]["pass-re"].value;

	if(password2=="" || password2.length < 6){
		alert("Password baru harus diisi dan terdiri dari 6 karakter atau lebih");
		return false;
	}
	else if(password1==password2 || password1==password3){
		alert("Password lama dengan password baru tidak boleh sama!");
		return false;
	}
	else if(password2!=password3){
		alert("Password baru tidak cocok dengan ketikan ulang!!");
		return false;
	}
	else if(password2==password3 && password1!="" && password2!=""){
		return true;
	}
	else if (password1==" " && password2==" " && password3== " "){
		alert("Password Tidak Boleh Kosong!!");
		return false;
	}
	else{
		return false;
	}
}

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function logout (status) {
	if (status == 1)
		alert("Anda berhasil logout!!");
	else
		;
}

function deleteConfirmUser (nama, nim)
{
	var check = confirm("Anda yakin ingin menghapus User : " + nama + " (NIM : " + nim + ")?");
	return check;
}

function deleteConfirmKarya (judul)
{
	var check = confirm("Anda yakin ingin menghapus karya : " + judul + " ?");
	return check;
}

function resetConfirm (nama, nim)
{
	var check = confirm("Anda yakin ingin mereset password User : " + nama + " (NIM : " + nim + ")?");
	return check;
}

function settingSuccess (id)
{
	if (id == 1)
		alert('Password berhasil diubah!!');
	else if (id == 2)
		alert('Profil berhasil diubah!!');
	else
		;
}

function adminActionSuccess (suc)
{
	if (suc == '1')
		alert("Password user berhasil direset menjadi 123456!!");
	else if (suc == '2')
		alert("User berhasil dihapus!!");
	else if (suc == '3')
		alert("User berhasil ditambahkan!!");
	else if (suc == '4')
		alert("User berhasil diubah!!");
	else if (suc == '5')
		alert("Karya berhasil dihapus!!");
	else if (suc == '6')
		alert("Karya berhasil diubah!!");
	else if (suc == '7')
		alert("Karya berhasil disetujui!!");
	else if (suc == '8')
		alert("Karya berhasil ditolak!!");
	else
		;
}

function userAction (id)
{
	if (id == '1')
		alert("Karya berhasil diupload! Karya anda akan muncul setelah administrator untuk menyetujui karya anda!");
	else if (id == '2')
		alert("Karya gagal diupload!!");
	else if (id == '3')
		alert("Karya berhasil dihapus!!");
	else
		;
}