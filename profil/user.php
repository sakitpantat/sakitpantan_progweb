<?php 
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != ""):
	{
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
		$database = "../database.php";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>
		<?php
			require_once($database);
			echo getName($nim);
		?> | Showcase Karya Mahasiswa FTI UKDW
	</title>
	<link rel="stylesheet" type="text/css" href="../stylesheet.css">
	<link rel="icon" href="../resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="../jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="../javascript.js"></script>
</head>
<body>
	<div class="header">
		<ul id="navleft">
			<li class="nav"><a href="../index.php">BERANDA</a></li>
			<li class="nav"><a href="../galeri.html">GALERI</a></li>
			<li class="nav"><a href="../musik.html">MUSIK</a></li>
			<li class="nav"><a href="../literatur.html">ARTIKEL</a></li>
			<li class="nav"><a href="../video.html">VIDEO</a></li>
			<li class="nav"><a href="../lain.html">LAINNYA</a></li>
		</ul>
		<ul id="navright">
			<input class="nav" type="text" name="search" placeholder="Masukkan kata kunci">
			<li class="nav">CARI</li>
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn nav">PROFIL</button>
				<div id="myDropdown" class="dropdown-content">
					<a href="profil.php?cat=">Profil</a>
					<a href="../setting.php?edit=akun&suc=">Setting</a>
					<a href="../logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="../index.php">PROFIL</a></li>
		</ul>
	</div>
	<div class="profil">
		<div class="foto">
			<?php printf('<a href="foto/%s.jpg"><img src="foto/%s.png" alt="PP"></a>', $nim, $nim)?>
			<!--<a class="ganti" href="71140000.html">GANTI FOTO PROFIL</a>-->
		</div>
		<div class="overhead">
			<h1><?php require_once($database); echo getName($nim);?></h1>
			<h3>NIM : <?php echo $nim; ?></h3>
			<h5><?php require_once($database); echo getQuotes($nim);?></h5><span class="ganti"><a href="../setting.php?edit=profil&suc=">EDIT QUOTE</a></span>
		</div>
		<div class="profil-karya">
			<div class="kategori1"><?php if ($_GET['cat'] == 'biodata') { echo '<a href="profil.php?cat=home">'; } else echo '<a href="profil.php?cat=biodata">';?>BIODATA</a></div>
			<div class="kategori1"><a href="profil.php?cat=karya">LIHAT KARYA</a></div>
			<div class="kategori1"><a href="profil.php?cat=upload">UPLOAD KARYA</a></div>
		</div>
		<?php
			if ($_GET['cat'] == 'biodata')
			{
				echo '<p>Email 1 : '; require_once($database); echo getEmail($nim).'</p>
				<p>Email 2 : '; require_once($database); if (getEmailSub($nim) == '') { echo 'Belum diisi. <a class="ganti" href="../setting.php?edit=profil&suc=">Isi disini</a>'; } else echo getEmailSub($nim); echo '</p>
				<p>Alamat : '; require_once($database); echo getAddress($nim).'</p>
				<p>Tanggal Lahir : '; require_once($database); echo getBirthday($nim).'</p>
				<p>Nomor HP : '; require_once($database); if (getNoHP($nim) == '') { echo 'Belum diisi. <a class="ganti" href="../setting.php?edit=profil&suc=">Isi disini</a>'; } else echo getNoHP($nim); echo'</p>
				<span><a class="ganti" href="setting.php?edit=profil&suc=">EDIT PROFIL</a></span>';
			}
			else if ($_GET['cat'] == 'karya')
			{
				echo '<table id="galeri-index">';
				echo '<tr class="galeri-image">';
				require_once("../database.php");
				$con = connect_database();
				$files = scandir("../galeri/design/");
				$hasil = mysqli_query($con, "SELECT * FROM files WHERE nim LIKE '$nim'");
				$hasil2 = mysqli_query($con, "SELECT * FROM biodatas WHERE nim LIKE $nim");
				$i = 0;
				while ($row = mysqli_fetch_assoc($hasil))
				{
					foreach ($files as $key => $file)
					{
						if($key == 0 || $key == 1) ;
						else
						{
							if ($file == $row['filename'])
							{
								echo "<td><img src='../galeri/design/".$file."'><br>".$row['nama']." oleh "; require_once("../database.php"); echo "<a href='profil.php?cat=' style='font-weight: bold;'>";require_once("../database.php"); echo getName($nim)."</a></td>";
							}
						}
					}
					$i++;
					if ($i == 5)
					{
						echo '</tr>';
					}
				}
				echo '</table>';
			}
			if ($_GET['cat'] == 'upload')
			{
				echo "<p>UKURAN MAKSIMAL 2 MB</p>";
				echo "<form action='../upload_karya.php' method='post' enctype='multipart/form-data'>";
				echo "<label>Judul Karya</label>";
				echo "<input type='text' name='nama_karya'><br>";
				echo "<label>Kategori</label>";
				echo "<select name='kategori'>";
				echo "<option value='Gambar'>Gambar</option>";
				echo "<option value='Musik'>Musik</option>";
				echo "<option value='Artikel'>Artikel</option>";
				echo "<option value='Lainnya'>Lainnya</option>";
				echo "</select><br>";
				echo "<label>Pilih file</label>";
				echo '<input type="file" accept="galeri/*" name="up"><br>';
				echo '<input type="submit">';
				echo '</form>';
			}
		?>
	</div>
    <footer>
		&copy;SakitPantat. <a href="tentang_kami.html">Tentang kami.</a> <a href="bantuan.html">Bantuan.</a>
	</footer>
</body>
</html>
<?php 
	else:
		header("Location: ../login.html");
	endif?>