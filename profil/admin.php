<?php 
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] == "admin"):
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
		$database = "../database.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>
		<?php if (isset($_GET['cat'])): ?>
			<?php if ($_GET['cat'] == 'user'): ?>
				User Account Manager | Showcase Karya Mahasiswa FTI UKDW 
			<?php elseif($_GET['cat'] == 'karya'): ?>
				Karya Manager | Showcase Karya Mahasiswa FTI UKDW 
			<?php endif ?>
		<?php else: ?>
			Admin Page | Showcase Karya Mahasiswa FTI UKDW 
		<?php endif ?>
	</title>
	<link rel="stylesheet" type="text/css" href="../stylesheet.css">
	<link rel="icon" href="../resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="../javascript.js"></script>
</head>
<body onload="adminActionSuccess('<?php if(isset($_GET['suc'])) echo $_GET['suc']; ?>')">
	<div class="header">
		<ul id="navleft">
			<li class="nav"><a href="../index.php">BERANDA</a></li>
			<li class="nav"><a href="../galeri.php">GALERI</a></li>
			<li class="nav"><a href="../musik.php">MUSIK</a></li>
			<li class="nav"><a href="../literatur.php">ARTIKEL</a></li>
			<li class="nav"><a href="../lain.php">LAINNYA</a></li>
		</ul>
		<ul id="navright">
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn"><?php require_once($database); echo getName($nim);?></button>
				<div id="myDropdown" class="dropdown-content">
					<a href="../setting.php?edit=akun">Setting</a>
					<a href="../logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="profil.php?cat=home"><?php require_once($database); echo getName($nim); ?></a></li>
		</ul>
	</div>
	<div class='profil'>
		<div class="profil-karya">
			<?php if (isset($_GET['cat'])): ?>
				<?php if ($_GET['cat'] == 'user'): ?>
					<div class="kategori1"><a href="admin.php">USER ACCOUNT</a></div>
				<?php else: ?>
					<div class="kategori1"><a href="admin.php?cat=user">USER ACCOUNT</a></div>
				<?php endif ?>
			<?php else: ?>
				<div class="kategori1"><a href="admin.php?cat=user">USER ACCOUNT</a></div>
			<?php endif ?>
			<?php if (isset($_GET['cat'])): ?>
				<?php if ($_GET['cat'] == 'karya'): ?>
					<div class="kategori1"><a href="admin.php">KARYA</a></div>
				<?php else: ?>
					<div class="kategori1"><a href="admin.php?cat=karya">KARYA</a></div>
				<?php endif ?>
			<?php else: ?>
				<div class="kategori1"><a href="admin.php?cat=karya">KARYA</a></div>
			<?php endif ?>
		</div>
			<?php 
				require_once($database);
				$con = connect_database();
				if (isset($_GET['cat'])):
					if ($_GET['cat'] == 'user'): ?>
					<div class="profil-karya">
					<div class="kategori1_2"><a href="admin.php?cat=user&do=add">TAMBAH USER</a></div>
					<?php
						if (isset($_GET['alert']))
						{
							echo '<span class="notif">';
							if ($_GET['alert'] == '1')
								echo "Password dan ketik ulang password tidak cocok!!";
							echo "</span>";
						}
					?>
					<?php if (isset($_GET['do'])) : ?>
						<?php if ($_GET['do'] == 'edit' && isset($_GET['id'])):
							require_once($database);
							$con = connect_database();
							$uid = $_GET['id'];
							$res = mysqli_query($con, "SELECT * FROM biodatas WHERE nim = '$uid';");
							$data = mysqli_fetch_assoc($res); ?>
							<form action="admin_action.php?do=3_2" onsubmit="return RepassValidator()" method="post" name="edit">
								<table>
									<thead class = "editprofile">
										<tr>
											<td><span>Edit User</span><br></td>
										</tr>
									</thead>
									<tbody class = "editprofile">
										<tr>
											<td><label>NIM</label></td>
											<td><input class = "isiform" type='text' name="nim" required value="<?php echo $data['nim']; ?>"><br></td>
										</tr>
										<tr>
											<td><label>Nama</label></td>
											<td><input class = "isiform" type='text' name="nama" required value="<?php echo $data['nama']; ?>"><br></td>
										</tr>
										<tr>
											<td><label>Alamat</label></td>
											<td><input class = "isiform" type='text' name="alamat" required value="<?php echo $data['alamat']; ?>"><br></td>
										</tr>
										<tr>
											<td><label>Email</label></td>
											<td><input class = "isiform" type='text' name="email" required value="<?php echo $data['email']; ?>"><br></td>
										</tr>
										<tr>
											<td><label>Tanggal lahir &nbsp</label></td>
											<td><input class = "isiform" type='text' name="date" required value="<?php echo $data['birthday']; ?>"><br></td>
										</tr>
										<tr>
											<td><br></td>
										</tr>
										<tr>
											<td><input type='submit' value="Submit"></td>
											<td><input type='reset'></td>
										</tr>
									</tbody>
								</table>	
							</form>
						<?php endif ?>
						<?php if ($_GET['do'] == "add"): ?>
							<form action="admin_action.php?do=3_1" method="post" name="add">
							<span>Tambah User</span><br>
							<label>NIM : </label>
							<input type='text' name="nim" required><br>
							<label>Nama : </label>
							<input type='text' name="nama" required><br>
							<label>Alamat : </label>
							<input type='text' name="alamat" required><br>
							<label>Email : </label>
							<input type='email' name="email" required><br>
							<label>Tanggal lahir : </label>
							<input type='text' name="date" required placeholder="YYYY-MM-YY"><br>
							<input type='submit' value="Submit">
							<input type='reset'>
							</form>
						<?php endif ?>
					<?php endif ?>
				<br><br><div class="overhead"><h1>USER ACCOUNT MANAGER</h1></div>
				<table class="table-normal">
					<thead>
						<td style="text-align:left">User ID (NIM)</td>
						<td style="text-align:left">Nama</td>
						<td style="text-align:left"># of Karya(s) Uploaded</td>
						<td colspan='3'>Aksi</td>
					</thead>
					<tbody>
						<?php
							$query = "SELECT * FROM users ORDER BY nim ASC";
							$res = mysqli_query($con, $query);
							$i = 0;
							while ($data = mysqli_fetch_assoc($res)):
						?>
						<?php if ($i % 2 == 0): ?>
							<tr class="odd">
						<?php else: ?>
							<tr class="even">
						<?php endif ?>
							<?php if ($data['nim'] != 'admin'):
								$namaUser = $data['nama'];
								$nimUser = $data['nim']; ?>
								<td style="text-align:left"><a href="profil.php?id=<?php echo $nimUser; ?>&cat=home" target="_blank"><?php echo $nimUser; ?></a></td>
								<td style="text-align:left"><a href="profil.php?id=<?php echo $nimUser; ?>&cat=home" target="_blank"><?php echo $namaUser; ?></a></td>
								<td style="text-align:right"><?php
									require_once($database);
									echo countUploaded($nimUser);
									$i++;
								?></td>
								<td><a href="admin_action.php?do=1&id=<?php echo $nimUser; ?>" onclick="return resetConfirm('<?php echo $namaUser; ?>','<?php echo $nimUser; ?>')">Reset<br>Password</a></td>
								<td><a href="admin.php?cat=user&do=edit&id=<?php echo $nimUser; ?>">Edit<br>Account</a></td>
								<td><a href="admin_action.php?do=2&id=<?php echo $nimUser; ?>" onclick="return deleteConfirmUser('<?php echo $namaUser; ?>', '<?php echo $nimUser; ?>')">Hapus<br>Account</a></td>
							<?php endif ?>
						</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
					<?php else: ?>
					<?php if ($_GET['cat'] == 'karya'): ?>
						<div class="profil-karya">
						<?php
						if (isset($_GET['alert']))
						{
							echo '<span class="notif">';
							if ($_GET['alert'] == '1')
								echo "Karya gagal dihapus!!";
							else if ($_GET['alert'] == '2')
								echo "Karya gagal diedit!!";
							echo "</span>";
						}
						?>
						<?php if (isset($_GET['do'])) : ?>
							<?php if ($_GET['do'] == 'edit' && isset($_GET['fid'])):
								require_once($database);
								$con = connect_database();
								$fid = $_GET['fid'];
								$res = mysqli_query($con, "SELECT * FROM files WHERE fid = '$fid';");
								$data = mysqli_fetch_assoc($res); ?>
								<form class = "editprofile" action="../edit_karya.php?fid=<?php echo $data['fid']; ?>" method="post" name="editk" enctype="multipart/form-data">
								<span class = "editprofile">Edit Karya</span><br>
								<label class = "editprofile">Judul </label>
								<input class = "isiform" type='text' name="nama" required value="<?php echo $data['nama']; ?>"><br>
								<label class = "editprofile">Kategori </label>
								<?php if ($data['kategori'] == 'Gambar'): ?>
									<input type='radio' name='kategori' value='Gambar' checked>Gambar
									<input type='radio' name='kategori' value='Musik'>Musik
									<input type='radio' name='kategori' value='Artikel'>Artikel
									<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>
								<?php elseif ($data['kategori'] == 'Artikel'): ?>
									<input type='radio' name='kategori' value='Gambar'>Gambar
									<input type='radio' name='kategori' value='Musik'>Musik
									<input type='radio' name='kategori' value='Artikel' checked>Artikel
									<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>
								<?php elseif ($data['kategori'] == 'Musik'): ?>
									<input type='radio' name='kategori' value='Gambar'>Gambar
									<input type='radio' name='kategori' value='Musik' checked>Musik
									<input type='radio' name='kategori' value='Artikel'>Artikel
									<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>
								<?php elseif ($data['kategori'] == 'Lainnya'): ?>
									<input type='radio' name='kategori' value='Gambar'>Gambar
									<input type='radio' name='kategori' value='Musik'>Musik
									<input type='radio' name='kategori' value='Artikel'>Artikel
									<input type='radio' name='kategori' value='Lainnya' checked>Lainnya<br>
								<?php else: ?>
									<input type='radio' name='kategori' value='Gambar'>Gambar
									<input type='radio' name='kategori' value='Musik'>Musik
									<input type='radio' name='kategori' value='Artikel'>Artikel
									<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>
								<?php endif; ?><br>
								<input type='submit' value="Submit">
								<input type='reset'>
								</form>
							<?php endif ?>
						<?php endif ?>
						<br><br><div class="overhead"><h1>KARYA USER MANAGER</h1></div>
						<table class="table-normal">
							<thead>
								<td style="text-align:left">ID Karya</td>
								<td style="text-align:left">Nama</td>
								<td style="text-align:left">NIM Author</td>
								<td style="text-align:left">Nama Author</td>
								<td style="text-align:left">Kategori</td>
								<td style="text-align:left">Status</td>
								<td colspan='2'>Aksi</td>
							</thead>
							<tbody>
								<?php
									$query = "SELECT * FROM files ORDER BY fid DESC";
									$res = mysqli_query($con, $query);
									$i = 0;
									while ($data = mysqli_fetch_assoc($res)):
								?>
								<?php if ($i % 2 == 0): ?>
									<?php if ($data['status'] == 'pending'): ?>
										<tr class="pending">
									<?php else: ?>
										<tr class="odd">
									<?php endif ?>
								<?php else: ?>
									<?php if ($data['status'] == 'pending'): ?>
										<tr class="pending">
									<?php else: ?>
										<tr class="even">
									<?php endif ?>
								<?php endif ?>
									<?php if ($data['nim'] != 'admin'): $namaKarya = $data['nama']; ?>
										<td style="text-align:right"><a href="../karya.php?fid=<?php echo $data['fid']; ?>" target="_blank"><?php echo $data['fid']; ?></a></td>
										<td style="text-align:left"><a href="../karya.php?fid=<?php echo $data['fid']; ?>" target="_blank"><?php echo $data['nama']; ?></a></td>
										<td style="text-align:left"><a href="profil.php?id=<?php echo $data['nim']; ?>&cat=home" target="_blank"><?php echo $data['nim']; ?></a></td>
										<td style="text-align:left"><a href="profil.php?id=<?php echo $data['nim']; ?>&cat=home" target="_blank"><?php
											require_once($database);
											echo getName($data['nim']);
										?></a></td>
										<td style="text-align:left"><?php echo $data['kategori']; ?></td>
										<td style="text-align:left"><?php echo $data['status']; $i++;?></td>
										<?php if ($data['status'] == 'pending'): ?>
											<td><a href="admin_action.php?do=4&fid=<?php echo $data['fid'];?>">Setujui<br>Karya</a></td>
											<td><a href="../hapus_karya.php?fid=<?php echo $data['fid'];?>&url=http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" onclick="return deleteConfirmKarya('<?php echo $namaKarya; ?>')">Tolak<br>Karya</a></td>
										<?php else: ?>
											<td><a href="admin.php?cat=karya&do=edit&fid=<?php echo $data['fid'];?>">Edit<br>Karya</a></td>
											<td><a href="../hapus_karya.php?fid=<?php echo $data['fid'];?>&url=http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" onclick="return deleteConfirmKarya('<?php echo $namaKarya; ?>')">Hapus<br>Karya</a></td>
										<?php endif ?>
									<?php endif ?>
								</tr>
								<?php endwhile; ?>
							</tbody>
						</table>
					<?php endif ?>
			<?php
					endif;
				endif;
			?>
		</div>
	</div>
</body>
</html>
<?php 
	else:
		header("Location:../login.html");
	endif?>