<?php 
	session_start();
	if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] != ""):
	{
		$nim = $_SESSION["NIM"];
		$pass = $_SESSION["PASS"];
		$database = "../database.php";
	}
	if (isset($_GET['id']))
	{
		if ($_GET['id'] == $nim)
			unset($_GET['id']);
		else
			$uid = $_GET['id'];
	}

	function getIcon($con, $uid, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE nim = '$uid' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		while ($data = mysqli_fetch_assoc($res))
		{
			$files = scandir("../".$data['scandir']);
			foreach ($files as $key => $file):
				if($key == 0 || $key == 1) ;
				else if ($file == $data['filename'])
					{echo "<td><a href = '../karya.php?fid=".$data['fid']."' target = '_blank'><img src='../".$data['icon']."'></a>";}
			endforeach;
		}
	}

	function getAuthors($con, $uid, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE nim = '$uid' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
			{
				echo "<td>Oleh : <a href='profil.php?id=".$data['nim']."&cat=home' style='font-weight: bold;'>";
				require_once("../database.php");
				echo getName($data['nim'])."</a></td>";
			}
		}
	}

	function getTitle($con, $uid, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE nim = '$uid' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
				echo "<td><a href='../karya.php?fid=".$data['fid']."' target='_blank'>".$data['nama']."</td>";
		}
	}

	function getCategory($con, $uid, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE nim = '$uid' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
			{
				echo "<td>Kategori : <a href='profil.php?id=".$data['kategori']."&cat=home' style='font-weight: bold;'>";
				require_once("../database.php");
				echo $data['kategori']."</a></td>";
			}
		}
	}

	function echoDelete($con, $uid, $off)
	{
		$res = mysqli_query($con, "SELECT * FROM `files` WHERE nim = '$uid' AND status = 'approved' ORDER BY `files`.`fid` DESC LIMIT 5 OFFSET $off");
		if (mysqli_num_rows($res) > 0)
		{
			while ($data = mysqli_fetch_assoc($res))
			{
				echo "<td><a href='../hapus_karya.php?fid=".$data['fid']."&cat=home'>";
				require_once("../database.php");
				echo "Hapus Karya</a></td>";
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>
		<?php
			require_once($database);
			if (isset($_GET['id']))
				echo getName($uid);
			else
				echo getName($nim);
		?> | Showcase Karya Mahasiswa FTI UKDW
	</title>
	<link rel="stylesheet" type="text/css" href="../stylesheet.css">
	<link rel="icon" href="../resources/favicon.png" type="image/png" sizes="16x16">
	<script type="text/javascript" src="../jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="../javascript.js"></script>
</head>
<body onload="userAction('<?php if (isset($_GET['suc'])) echo $_GET['suc']; ?>')">
	<div class="header">
		<ul id="navleft">
			<li class="nav"><a href="../index.php">BERANDA</a></li>
			<li class="nav"><a href="../galeri.php">GALERI</a></li>
			<li class="nav"><a href="../musik.php">MUSIK</a></li>
			<li class="nav"><a href="../literatur.php">ARTIKEL</a></li>
			<li class="nav"><a href="../lain.php">LAINNYA</a></li>
		</ul>
		<ul id="navright">
			<input class="nav" type="text" name="search" placeholder="Masukkan kata kunci">
			<li class="nav">CARI</li>
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn nav"><?php require_once($database); echo getName($nim); ?></button>
				<div id="myDropdown" class="dropdown-content">
					<a href="profil.php?cat=">Profil</a>
					<a href="profil.php?cat=upload">Upload Karya</a>
					<a href="../setting.php?edit=akun&suc=">Setting</a>
					<a href="../logout.php">Logout</a>
				</div>
			</div>
			<li class="nav"><a href="profil.php?cat=home"><?php require_once($database); echo getName($nim); ?></a></li>
		</ul>
	</div>
	<div class="profil">
		<div class="foto">
			<?php
				require_once($database);
				$con = connect_database();
				if (isset($_GET['id'])):
				$query = "SELECT * FROM users WHERE nim = '$uid'";
				$res = mysqli_query($con, $query);
				$user = mysqli_fetch_assoc($res); ?>
				<a href="foto/<?php echo $user['pp']; ?>" target="_blank"><img src="foto/<?php echo $user['pp']; ?>" alt="PP"></a>
			<?php else:
				$query = "SELECT * FROM users WHERE nim = '$nim'";
				$res = mysqli_query($con, $query);
				$user = mysqli_fetch_assoc($res); ?>
				<a href="foto/<?php echo $user['pp']; ?>" target="_blank"><img src="foto/<?php echo $user['pp']; ?>" alt="PP"></a>
				<span class="ganti"><a href="../setting.php?edit=profil&suc=">GANTI FOTO PROFIL</a></span>
			<?php endif ?>
		</div>
		<div class="overhead">
			<?php if (isset($_GET['id'])): ?>
				<h1><?php require_once($database); echo getName($uid);?></h1>
				<h3>NIM : <?php echo $uid; ?></h3>
				<h5><?php require_once($database); echo getQuotes($uid);?></h5>
			<?php else: ?>
				<h1><?php require_once($database); echo getName($nim);?></h1>
				<h3>NIM : <?php echo $nim; ?></h3>
				<h5><?php require_once($database); echo getQuotes($nim);?></h5><span class="ganti"><a href="../setting.php?edit=profil&suc=">EDIT QUOTE</a></span>
			<?php endif ?>
		</div>
		<div class="profil-karya">
			<?php if (isset($_GET['id'])): ?>
				<div class="kategori1"><?php if ($_GET['cat'] == 'biodata') { echo '<a href="profil.php?id='.$uid.'&cat=home">'; } else echo '<a href="profil.php?id='.$uid.'&cat=biodata">';?>BIODATA</a></div>
				<div class="kategori1"><a href="profil.php?id=<?php echo $uid; ?>&cat=karya">LIHAT KARYA</a></div>
			<?php else: ?>
				<div class="kategori1"><?php if ($_GET['cat'] == 'biodata') { echo '<a href="profil.php?cat=home">'; } else echo '<a href="profil.php?cat=biodata">';?>BIODATA</a></div>
				<div class="kategori1"><a href="profil.php?cat=karya">LIHAT KARYA</a></div>
				<div class="kategori1"><a href="profil.php?cat=upload">UPLOAD KARYA</a></div>
			<?php endif ?>
		</div>
		<?php
			if (isset($_GET['id']))
			{
				if ($_GET['cat'] == 'biodata')
				{
					echo '<p>Email 1 : '; require_once($database); echo getEmail($uid).'</p>';
					require_once($database); if (getEmailSub($uid) == '') ; else echo '<p>Email 2 : '.getEmailSub($uid); echo '</p>
					<p>Alamat : '; require_once($database); echo getAddress($uid).'</p>
					<p>Tanggal Lahir : '; require_once($database); echo getBirthday($uid).'</p>
					<p>Nomor HP : '; require_once($database); if (getNoHP($uid) == '') ; else echo getNoHP($uid); echo'</p>';
				}
				else if ($_GET['cat'] == 'karya')
				{
					require_once($database);
					$con = connect_database();
					echo '<table id="artikel-index"><tbody>';
					$res = mysqli_query($con, "SELECT COUNT(fid) FROM files WHERE nim = '$uid' AND status = 'approved'");
					require_once($database);
					$uploaded = countUploaded($uid);
					if ($uploaded <= 0)
						echo "<tr class='artikel-index'><td>Belum ada karya yang diupload</td></tr>";
					else
					{
						while ($uploaded > 0)
						{
							require_once($database);
							$off = countUploaded($uid) - $uploaded;
							echo "<tr class='galeri-image'>"; getIcon($con, $uid, $off); echo "</tr>";
							echo "<tr class='showcase-title'>"; getTitle($con, $uid, $off); echo "</tr>";
							echo "<tr class='showcase-author'>"; getAuthors($con, $uid, $off); echo "</tr>";
							echo "<tr class='showcase-author'>"; getCategory($con, $uid, $off); echo "</tr>";
							$uploaded-=5;
						}
					}
					echo '</tbody></table>';
				}
			}
			else
			{
				if ($_GET['cat'] == 'biodata')
				{
					echo '<p>Email 1 : '; require_once($database); echo getEmail($nim).'</p>
					<p>Email 2 : '; require_once($database); if (getEmailSub($nim) == '-') { echo 'Belum diisi. <a class="ganti" href="../setting.php?edit=profil&suc=">Isi disini</a>'; } else echo getEmailSub($nim); echo '</p>
					<p>Alamat : '; require_once($database); echo getAddress($nim).'</p>
					<p>Tanggal Lahir : '; require_once($database); echo getBirthday($nim).'</p>
					<p>Nomor HP : '; require_once($database); if (getNoHP($nim) == '-') { echo 'Belum diisi. <a class="ganti" href="../setting.php?edit=profil&suc=">Isi disini</a>'; } else echo getNoHP($nim); echo'</p>
					<span><a class="ganti" href="../setting.php?edit=profil&suc=">EDIT PROFIL</a></span>';
				}
				else if ($_GET['cat'] == 'karya')
				{
					require_once($database);
					$con = connect_database();
					echo '<table id="artikel-index"><tbody>';
					$res = mysqli_query($con, "SELECT COUNT(fid) FROM files WHERE '$nim' AND status = 'approved'");
					require_once($database);
					$uploaded = countUploaded($nim);
					if ($uploaded <= 0)
						echo "<tr class='artikel-index'><td>Anda belum pernah mengupload karya. <a href='profil.php?cat=upload' style='text-decoration:underline;'>Mulai upload?</a></td></tr>";
					else
					{
						while ($uploaded > 0)
						{
							require_once($database);
							$off = countUploaded($nim) - $uploaded;
							echo "<tr class='galeri-image'>"; getIcon($con, $nim, $off); echo "</tr>";
							echo "<tr class='showcase-title'>"; getTitle($con, $nim, $off); echo "</tr>";
							echo "<tr class='showcase-author'>"; getAuthors($con, $nim, $off); echo "</tr>";
							echo "<tr class='showcase-author'>"; getCategory($con, $nim, $off); echo "</tr>";
							echo "<tr class='showcase-author'>"; echoDelete($con, $nim, $off); echo "</tr>";
							$uploaded-=5;
						}
					}
					echo '</tbody></table>';
				}
				if ($_GET['cat'] == 'upload')
				{
					echo "<form action='../upload_karya.php' method='post' enctype='multipart/form-data'>";
					echo "<label>Judul Karya : </label>";
					echo "<input class = 'isiform' type='text' name='nama_karya' required><br>";
					echo "<label>Kategori : </label>";
					if (isset($_GET['ref'])) {
						if ($_GET['ref'] == 'gambar')
						{
							echo "<input type='radio' name='kategori' value='Gambar' checked>Gambar";
							echo "<input type='radio' name='kategori' value='Musik'>Musik";
							echo "<input type='radio' name='kategori' value='Artikel'>Artikel";
							echo "<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>";
						}
						else if ($_GET['ref'] == 'artikel')
						{
							echo "<input type='radio' name='kategori' value='Gambar'>Gambar";
							echo "<input type='radio' name='kategori' value='Musik'>Musik";
							echo "<input type='radio' name='kategori' value='Artikel' checked>Artikel";
							echo "<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>";
						}
						else if ($_GET['ref'] == 'musik')
						{
							echo "<input type='radio' name='kategori' value='Gambar'>Gambar";
							echo "<input type='radio' name='kategori' value='Musik' checked>Musik";
							echo "<input type='radio' name='kategori' value='Artikel'>Artikel";
							echo "<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>";
						}
						else
						{
							echo "<input type='radio' name='kategori' value='Gambar'>Gambar";
							echo "<input type='radio' name='kategori' value='Musik'>Musik";
							echo "<input type='radio' name='kategori' value='Artikel'>Artikel";
							echo "<input type='radio' name='kategori' value='Lainnya' checked>Lainnya<br>";
						}
					}
					else
					{
						echo "<input type='radio' name='kategori' value='Gambar'>Gambar";
						echo "<input type='radio' name='kategori' value='Musik'>Musik";
						echo "<input type='radio' name='kategori' value='Artikel'>Artikel";
						echo "<input type='radio' name='kategori' value='Lainnya'>Lainnya<br>";
					}
					echo "<label>Pilih file (UKURAN MAKSIMAL 2 MB) : </label>";
					echo '<input type="file" accept="galeri/*" name="up" required><br>';
					echo "<label>Deskripsi : </label>";
					echo '<textarea name="desc" required></textarea><br>';
					echo '<input type="submit">';
					echo '</form>';
				}
			}			
		?>
	</div>
    <footer>
		&copy;SakitPantat. <a href="tentang_kami.html">Tentang kami.</a> <a href="bantuan.html">Bantuan.</a>
	</footer>
</body>
</html>
<?php 
	else:
		header("Location: ../login.html");
	endif?>